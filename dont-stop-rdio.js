;(function(doc , w) {
    'use strict';
	
    var $resumePlaybackButton;
	
	var dontStopRdio = function() {
		
        var $resumePlaybackButton = doc.querySelectorAll( '.Dialog_MessageDialog .Dialog_ButtonBar button[data-cid]');

        if($resumePlaybackButton.length !== 2) {
            return;
        }
		
        $resumePlaybackButton[1].click();

		jQuery.when(w.R.Services.ready("Loader"), w.R.Services.ready("Updater")).done(function(){
            w.R.loader.load(["Desktop.Main"], function() {
                w.R.player._playAnAd();
            });
        });
    };
	
    w.setInterval(dontStopRdio, 2500);

})( document, window );